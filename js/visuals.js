angular.module('visuals', [])
.directive('bubbleMap', function () { 
  // constants
  var width = 960,
      height = 700;

  return {
    restrict: 'E',
    scope: {
      data: '=data'
    },
    link: function (scope, element, attrs) {
      var dc = scope.data;

      // set up initial svg object
      var svg = d3.select(element[0])
        .append("svg")
        .attr("width", width)
        .attr("height", height);

      // center point gathered from json data
      var projection = d3.geo.mercator()
        .center([-77.114263, 38.817987])
        .scale(170 * width) // makes sure we can see the map
        .translate([ width / 4, height]);

      //Define path generator
      var path = d3.geo.path()
        .projection(projection);

      // tooltip popup helper class setup
      var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d) {
            return "<strong>" + d.properties.name +": </strong> <span style='color:red'>" + d.properties.total + " incidents</span>";
        });
     
      // radius generator base on fed value
      var radius = d3.scale.sqrt()
        .domain([0, 50])
        .range([0, 15]);

      // draw city land
      svg.append("path")
        .datum(topojson.feature(dc, dc.objects.annotatedData))
        .attr("class", "city")
        .attr("d", path);

      // draw circles based on total incidents at the center of each city polygon
      var circles = svg.append("g")
        .attr("class", "bubble")
        .selectAll("circle")
          .data(topojson.feature(dc, dc.objects.annotatedData).features)
        .enter().append("circle")
          .attr("transform", function(d) { return "translate(" + path.centroid(d) + ")"; })
          .attr("r", function(d) { return radius(d.properties.total); })
        .append("title")
          .text(function(d) {
          return d.properties.name;                
        })
        .call(tip);

      // Draw city borders.
      svg.append("path")
          .datum(topojson.mesh(dc, dc.objects.annotatedData, function(a, b) { return a !== b; }))
          .attr("class", "city-border")
          .attr("d", path);

      // add event listeners when hovering circles
      svg
        .selectAll("circle")
        .on('mouseover', tip.show)
        .on('mouseout', tip.hide);
    }
  }
})
.directive('densityMap', function () {

  // constants
  var width = 960,
      height = 700;

  return {
    restrict: 'E',
    scope: {
      data: '=data',
      filter: '=filter'
    },

    link: function (scope, element, attrs) {
      var dc = scope.data;

      // returns a color depending on the fed value 
      var color = d3.scale.threshold()
        .domain([1, 5, 10, 15, 20, 25, 30, 35])
        .range(["#fff7ec", "#fee8c8", "#fdd49e", "#fdbb84", "#fc8d59", "#ef6548", "#d7301f", "#b30000", "#7f0000"]);

      // set up initial svg object
      var svg = d3.select(element[0])
        .append("svg")
        .attr("width", width)
        .attr("height", height);

      // center point gather from json data
      var projection = d3.geo.mercator()
        .center([-77.114263, 38.817987])
        .scale(170 * width)
        .translate([ width / 4, height]);

      //Define path generator
      var path = d3.geo.path()
            .projection(projection);

      var render = function (filter) {
        // draws city land and group then by color base on specified filter
        svg.append('g')
        .attr("class", "dc")
        .selectAll("path")
          .data(d3.nest()
            .key(function (d) { return color(d.properties[filter]); })
            .entries(dc.objects.annotatedData.geometries))
          .enter().append("path")
            .attr("class", "city")
            .style("fill", function(d) { return d.key; })
            .attr("d", function(d) { return path(topojson.merge(dc, d.values)); });
            

        // draws city borders
        svg.append("path")
          .datum(topojson.mesh(dc, dc.objects.annotatedData, function(a, b) { return a !== b; }))
          .attr("class", "city-border")
          .attr("d", path);
      };

      // event listener to watch for filter value (total, 2010, ...)
      scope.$watch('filter', function (newVal, old) {
        svg.selectAll('g').data([]).exit().remove();
        svg.selectAll('path').data([]).exit().remove();
        render(newVal);
      });
    }
  }
})
.directive('barChart', function () {
  return {
    restrict: 'E',
    scope: {
      data: '=data'
    },

    link: function (scope, element, attrs) {
      var data = scope.data;

      var margin = {
        top: 80, 
        right: 180, 
        bottom: 80, 
        left: 180
      },
      width = 960 - margin.left - margin.right,
      height = 500 - margin.top - margin.bottom;

      // set ups x & y positions  
      var x = d3.scale.ordinal()
          .rangeRoundBands([0, width], .1, .3);

      var y = d3.scale.linear()
          .range([height, 0]);
      
      var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom");

      var yAxis = d3.svg.axis()
          .scale(y)
          .orient("left")
          .ticks(8, "");

      // creates svg using directive element
      var svg = d3.select(element[0])
          .append("svg")
          .attr("width", width + margin.left + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // parses year data
      x.domain(data.map(function(d) { return d.year; }));
      // parses total data
      y.domain([0, d3.max(data, function(d) { return d.total; })]);

      // draws xAxis
      svg.append("g")
          .attr("class", "x axis")
          .attr("transform", "translate(0," + height + ")")
          .call(xAxis);

      // draws yAxis
      svg.append("g")
          .attr("class", "y axis")
          .call(yAxis);

      // plot data
      svg.selectAll(".bar")
          .data(data)
        .enter().append("rect")
          .attr("class", "bar")
          .attr("x", function(d) { return x(d.year); })
          .attr("width", x.rangeBand())
          .attr("y", function(d) { return y(d.total); })
          .attr("height", function(d) { return height - y(d.total); });  
    }
  }
});