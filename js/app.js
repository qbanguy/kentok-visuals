var kentokApp = angular.module('kentok', ['ui.router', 'visuals', 'ngAnimate']);

kentokApp.controller('MainController', function ($scope, $state) {
  // default report state when app is initially launched
  $state.go('report');
});

kentokApp.controller('BarChartController', function ($scope, resp) {
  var years = [2010, 2011, 2012, 2013, 2014];

  var summary = _.map(years, function (y) {
    return {
      year: y,
      total: 0
    }
  });

  // perform total addition for a particular year
  var summarize = function (year, count) {
    _.each(summary, function (obj) {
      if (year === obj.year && parseInt(count) !== NaN) {
        return obj.total += count;
      }
    });
  }

  // main 2d loop to gather all years per feature
  _.each(resp.data.features, function (d, i) {
    _.each(years, function (y) {
      summarize(y, d.properties[y]);
    });
  });

  $scope.data = summary;
});

kentokApp.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state("bubblemap", {
      url: "/bubblemap",
      templateUrl: "partials/bubble-map.html",
      controller: function ($scope, resp) {
        $scope.data = resp.data;
      },
      resolve: {
        resp: function ($http) {
          return $http({ method: 'GET', url: 'impaired.json' });
        }
      }
    })
    .state("density", {
      url: "/density",
      templateUrl: "partials/density.html",
      controller: function ($scope, resp) {
        $scope.filterBy = 'total';
        $scope.data = resp.data;
      },
      resolve: {
        resp: function ($http) {
          return $http({ method: 'GET', url: 'impaired.json' });
        }
      }
    })
    .state("report", {
      url: "/report",
      templateUrl: "partials/report.html" ,
      controller: function ($scope, resp) {
        $scope.data = resp.data.features.map(function (d) {
          return _.pick(d.properties, ['name', 'total', '2010', '2011', '2012', '2013', '2014', 'objectid']);
        });

        $scope.data.sort(function(a, b) { return b.total - a.total; });
        var currData = $scope.data;

        $scope.doQuery = function () {
          $scope.data = _.filter(currData, function (d) {
            return d.name.toLowerCase().indexOf($scope.query.toLowerCase()) > -1
          });
        }

      },
      resolve: {
        resp: function ($http) {
          return $http({ method: 'GET', url: 'annotatedData.geojson' });
        }
      }
    })
    .state("barchart", {
      url: "/barchart",
      templateUrl: "partials/bar-chart.html",
      controller: "BarChartController",
      resolve: {
        resp: function ($http) {
          return $http({ method: 'GET', url: 'annotatedData.geojson' });
        }
      }
    });
});